################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

from readData import ReadMCData
from convertBinaryGraph import ConvertBinaryGraph
from graph import graph
import sys

convertor = ConvertBinaryGraph(0)  # 0.15 the best so far

convertor.binaryAdjMatrixGen(100,50)

# write each graph to json, create a dataset for graph2vec
labelFile = open('../dataset/simulatedLabel.csv', "r")
gnnInFile = open('./simuGnnIn.txt', "a")

for i in range(len(convertor.Xb)):

    Gp = graph(convertor.Xb[i], [], None)
    if i == 0:
        Gp.ToGNNinputNumOfGraph(gnnInFile, len(convertor.Xb))
    # Gp.ToGNNinput(gnnInFile,labelFile)
    Gp.ToGNNinput100(gnnInFile, labelFile)

labelFile.close()
gnnInFile.close()

print("done")