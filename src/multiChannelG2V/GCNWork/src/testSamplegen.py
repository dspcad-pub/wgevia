################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import json
import csv
import numpy as np

class SampleGen:

    def __init__(self, size):
        self.size = size
    def graphGen(self):

        for i in range(self.size):
            f = open("../dataset/genSample/" + str(i) + ".json", "w+")

            if(i<self.size/2):
                nodes = np.random.choice(101, 5, replace=False)

                f.write("{\"edges\": [")
                f.write("["+str(nodes[0])+", "+str(nodes[1])+"]], ")

                f.write("\"features\": {")

                f.write("\"" + str(nodes[0]) + "\": \"" + str(1) + "\", ")
                f.write("\"" + str(nodes[1]) + "\": \"" + str(1) + "\", ")
                f.write("\"" + str(nodes[2]) + "\": \"" + str(0) + "\", ")
                f.write("\"" + str(nodes[3]) + "\": \"" + str(0) + "\", ")

                f.write("\"" + str(nodes[4]) + "\": \"" + str(0) + "\"}}")
            else:
                nodes = np.random.choice(101, 5, replace=False)

                f.write("{\"edges\": [")
                f.write("[" + str(nodes[0]) + ", " + str(nodes[1]) + "], ")
                f.write("[" + str(nodes[1]) + ", " + str(nodes[2]) + "], ")
                f.write("[" + str(nodes[2]) + ", " + str(nodes[3]) + "], ")
                f.write("[" + str(nodes[3]) + ", " + str(nodes[4]) + "]], ")

                f.write("\"features\": {")

                f.write("\"" + str(nodes[0]) + "\": \"" + str(1) + "\", ")
                f.write("\"" + str(nodes[1]) + "\": \"" + str(2) + "\", ")
                f.write("\"" + str(nodes[2]) + "\": \"" + str(2) + "\", ")
                f.write("\"" + str(nodes[3]) + "\": \"" + str(2) + "\", ")

                f.write("\"" + str(nodes[4]) + "\": \"" + str(1) + "\"}}")

            f.close()


    def labelgen(self):

        with open("../dataset/genSample/labelAll.csv", 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile)
            for i in range(self.size):
                if(i<self.size/2):
                    spamwriter.writerow(str(1))
                else:
                    spamwriter.writerow(str(2))


sg = SampleGen(1200)
sg.graphGen()
sg.labelgen()
print('done')