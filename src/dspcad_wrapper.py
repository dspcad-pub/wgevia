################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import yaml
import os
#read yaml file
with open('config.yaml') as file:
    config = yaml.safe_load(file)
    print(config)

    if config["workers"] != None:
        os.system("python3 main.py --opcode "+config["opcode"]+" --workers "+str(config["workers"])+" --nT "+str(config["nT"])+" --d "+str(config["d"])+" --w "+str(config["w"]))
    else:

        if config["d"] !=None and config["nT"] !=None and config["w"] !=None:
            os.system("python3 main.py --opcode "+config["opcode"]+" --nT "+str(config["nT"])+" --d "+str(config["d"])+" --w "+str(config["w"]))

        if config["d"] !=None and config["nT"] !=None and config["w"] ==None:
            os.system("python3 main.py --opcode "+config["opcode"]+" --nT "+str(config["nT"])+" --d "+str(config["d"]))

        if config["d"] ==None and config["nT"] ==None and config["w"] ==None:
            os.system("python3 main.py --opcode "+config["opcode"])