################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.datasets import mnist
from readData import ReadFLData
from DataLoader import DataLoader
from sklearn import metrics
import sys
import logging
logging.getLogger('tensorflow').disabled = True

"""
Python script for visualization of one channel's feature
"""

# Read input_data.
LD = ReadFLData()
ld = DataLoader()
Yp = '../labels/labelAllr2.csv'
#Yp = 'labelAlls.csv'

import time
from sklearn.manifold import TSNE
#take input from cmd

Xp = '../singleFeatures/g2vt0.csv' #input file with path

X,Y = LD.loadXY(Xp,Yp)
Y = Y.reshape((Y.shape[0],1))
XY = np.concatenate((X,Y),axis=1)
X,Y = ld.splitXY(XY)

#n_sne = 7000

time_start = time.time()
tsne = TSNE(n_iter=300,n_components=2)
tsne_results = tsne.fit_transform(X)

print ('t-SNE done! Time elapsed: {} seconds'.format(time.time()-time_start))

label = Y
silhScore = metrics.silhouette_score(tsne_results, label, metric='sqeuclidean')
print("the silhouette score is: " + str(silhScore))


# Create the figure
fig = plt.figure( figsize=(8,8) )
ax = fig.add_subplot(1, 1, 1, title='TSNE' )
# Create the scatter
ax.scatter(
    x=tsne_results[:,0],
    y=tsne_results[:,1],
    c=label,
    cmap=plt.cm.get_cmap('Paired'),
    alpha=0.4)
plt.savefig('../plotReal2/combinedREAL2.png')
plt.show()
