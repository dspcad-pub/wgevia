################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import sys
import os
import DataLoader
import bigGraph
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from loadGraph import loadGraph

"""
Python script for generating aggregated graphs for visualization
"""

inputFolderPath = '../../MCSWE1.1origin/graphOut/json/'
labels = '../../MCSWE1.1origin/mcDataset/csvsReal2/labelAll.csv'
#outputFile = sys.argv[2]

numOfGraph = len(os.listdir(inputFolderPath))
ld = DataLoader.DataLoader()

loader = loadGraph()
Y = ld.loadData(labels)
numOfNode = 420
bg = bigGraph.bigGraph(numOfNode,Y)
for i in range(numOfGraph):
    graph,features = loader.load(inputFolderPath+"graph_"+str(i)+".json")
    bg.aggregate(graph,i)

g0,g1 = bg.avg()
s1 = np.sum(g0)
s2 = np.sum(g1)
print("graph labeled 0 has edges: "+str(s1))
print("graph labeled 1 has edges: "+str(s2))


G0 = nx.from_numpy_matrix(g0)
G1 = nx.from_numpy_matrix(g1)

nx.draw_circular(G0, with_labels = True,font_size = 6,node_size = 50,edge_color = 'g',node_color='y')
plt.savefig("../visOut/l0.png") # save as png
plt.show() # display

nx.draw_circular(G1, with_labels = True,font_size = 6,node_size = 50,edge_color = 'g',node_color='y')
plt.savefig("../visOut/l1.png") # save as png
plt.show() # display

print("done")