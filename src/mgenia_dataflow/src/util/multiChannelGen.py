################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import os
import sys
from g2vpre import findRange


class multiChannelGen():
    def __init__(self,threshold,posT):
        #inputs
        self.reader = None
        self.posT = posT
        self.Threshold = threshold
        #variables
        self.top = None
        self.bot = None
        #output
        self.step = None
        self.Ts = []

    def load(self,reader):
        self.reader = reader


    def run(self):
        self.top, self.bot = findRange(self.reader.X)
        print("got top " + str(self.top))
        print("got bot " + str(self.bot))

        if self.posT == 'posT':
            print("posT")
            posT = True
        else:
            print("negT")
            posT = False

        if posT != True:
            self.step = (float(self.top) - float(self.bot)) / (int(self.Threshold))  # negT
        else:
            self.step = float(self.top) / (int(self.Threshold))  # posT

        for i in range(int(self.Threshold)):
            if posT != True:
                self.Ts.append(float(self.bot) + i * self.step)  # negT
            else:
                self.Ts.append(i * self.step)  # posT

        # clean previous g2vi.csv files and json folder of previous run
        os.system("cd ../../channels\nrm -rf [a-zA-Z]*[0-9]")
        os.system("cd ../../channels/g2vFeatures\nrm g* c*")








