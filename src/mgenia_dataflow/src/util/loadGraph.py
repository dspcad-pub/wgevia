################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import os
import sys
from readData import ReadMCData


class loadGraph():
    def __init__(self, isTrain = True):
        self.isTrain = isTrain
        self.reader = None
        self.path = None
        #self.load()

    def load(self,path):
        self.path = path

    def run(self):
        #count number of files, should be equal or larger than files existed
        filenum = len(os.listdir(self.path))#'../../weightedGraphs/'
        print("num of files: "+str(int(filenum)))
        if filenum <= 2:
            print("ERROR: not enough input files")
        else:
            if self.isTrain:
                #remove previous saved model from train mode
                os.system("cd ../../trainedModel\nrm d2v*")

            self.reader = ReadMCData('04', int(filenum), '../../weightedGraphs/')
            self.reader.readX()
            print("done read X")
            #not necessary if user provided labelAll.csv
            #reader.readY()
            #print("done read Y")
            #not necessary if user provided labelAll.csv
            #reader.writeY()

            numOfGraph = len(self.reader.X)
            print("amount of input graphs: "+str(numOfGraph))

if __name__ == "__main__":
    loader = loadGraph()
