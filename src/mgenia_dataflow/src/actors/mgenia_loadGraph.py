################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import sys
sys.path.append("../util/")
sys.path.append("../../wrapped/welter/exp/lang/python/src/gems/actors/common/")
sys.path.append("../../wrapped/welter/exp/lang/python/src/gems//edges/")
from welt_py_actor import Actor
from loadGraph import loadGraph
from welt_py_fifo_basic import welt_py_fifo_basic_new

class mgenia_loadGraph(Actor):
    def __init__(self,pathqin,readerqout,isTrain = True):
        super().__init__(index=0, mode="COMMON")
        self.pathqin = pathqin
        self.readerqout = readerqout
        self.loader = loadGraph(isTrain)

    def enable(self):
        if self.pathqin.welt_py_fifo_basic_population() > 0:
            if self.readerqout.welt_py_fifo_basic_population() == 0:
                return True
            else:
                return False
        else:
            return False

    def invoke(self):
        #check
        print("loadGraph actor invokes")
        path = self.pathqin.welt_py_fifo_basic_read_direct()
        self.loader.load(path)
        self.loader.run()

        self.readerqout.welt_py_fifo_basic_write_ref(self.loader.reader)


    def terminate(self):
        pass

#unit test

if __name__ == "__main__":
    fifo_test = welt_py_fifo_basic_new(1, 0)
    actor_lg = mgenia_loadGraph(fifo_test,isTrain=True)
    if actor_lg.enable():
        actor_lg.invoke()
    print(fifo_test)