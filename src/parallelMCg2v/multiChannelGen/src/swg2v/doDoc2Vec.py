################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

from gensim.models.doc2vec import Doc2Vec

class doDoc2Vec(object):
    def __init__(self,graphCollection,numOfGraph,Dim,downSamplingRate,learnRate):
        self.graphCollection = graphCollection
        self.Dim = Dim
        self.learnRate = learnRate
        self.downSamplingRate = downSamplingRate
        self.model = None
        self.graphsRepresent = []
        self.numOfGraph = numOfGraph

    def run(self):
        self.model = Doc2Vec(self.graphCollection,
            vector_size=self.Dim,
            window=0,
            min_count=1,
            dm=0,
            sample=self.downSamplingRate,
            workers=1,
            epochs=100,
            alpha=self.learnRate)

    def saveRepresent(self):
        for i in range(self.numOfGraph):
            self.graphsRepresent.append(list(self.model.docvecs["graph_"+str(i)]))

    def getRepresent(self):
        return self.graphsRepresent
