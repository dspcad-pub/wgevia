################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import hashlib

class graphFeatureGen(object):
    def __init__(self,graph,features,iter):
        self.allFeatures = features
        self.iter = iter
        self.features = features #sequenced features, natural ordering
        self.graph = graph
        self.featurescpy = []
        for each in features:
            self.featurescpy.append(each)

    def wlIterations(self):
        for i in range(self.iter):
            self.perWlIteration()
        return self.allFeatures

    def perWlIteration(self):
        featuresCurrIter = []
        for i in range(len(self.graph.nodes())):
            neighbors = self.graph.neighbors(i)
            feature = []
            for each in neighbors:
                feature.append(self.features[each])
            list = []
            if len(feature) != 0:
                list = [self.features[i]]+sorted(feature)
                features = "_".join(list)
                hash_object = hashlib.md5(features.encode())
                uniqueRepresent = hash_object.hexdigest()
                featuresCurrIter.append(uniqueRepresent)
                self.featurescpy[i] = uniqueRepresent
        try:
            self.allFeatures = self.allFeatures + featuresCurrIter
        except:
            print(self.allFeatures)
            print(featuresCurrIter)

        for i in range(len(self.features)):
            self.features[i] = self.featurescpy[i]