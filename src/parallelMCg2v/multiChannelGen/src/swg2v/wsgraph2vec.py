################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import sys
import os
from docGen import docGen
from graphFeatureGen import graphFeatureGen
from loadGraph import loadGraph
from represent import represent
from doDoc2Vec import doDoc2Vec

Dim = int(sys.argv[1])
wlIter = int(sys.argv[2])
inputFolderPath = sys.argv[3]
outputFile = sys.argv[4]
learnRate = 0.025
downSampleRate = 0.0001
numOfGraph = len(os.listdir(inputFolderPath))

loader = loadGraph()
docGener = docGen()

for i in range(numOfGraph):
    graph,features = loader.load(inputFolderPath+"graph_"+str(i)+".json")
    graphDoc = graphFeatureGen(graph,features,wlIter)
    docGener.addDoc(graphDoc.wlIterations(),i)
    #print("\rgraph_"+str(i)+".json docgen finished")
print("docGen finished")
Doc2vec = doDoc2Vec(docGener.getDoc(),numOfGraph,Dim,downSampleRate,learnRate)
Doc2vec.run()
Doc2vec.saveRepresent()
outwriter = represent(Doc2vec.getRepresent(),numOfGraph,outputFile)
outwriter.output()
print("weighted sparsed graph2vec done")
