################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

from readData import ReadMCData
from convertBinaryGraph import ConvertBinaryGraph
from graph import graph
import sys
import os

#input format: -threshold FLOAT -jsonfolder STR
Threshold = float(sys.argv[1]) #0.15
jsonfolder = sys.argv[2] # "../dataset/json/"
embfolder = "../dataset/emb/"
n2vfeaturefolder = "../dataset/n2vFeatures/"

reader = ReadMCData('04', 9001, '../dataset/csvsReal/')
reader.readX()
reader.readY()
reader.writeY()
#reader.readXtest()
#reader.writeXcsv()
print("current Threshold: "+str(Threshold)+"\n")
convertor = ConvertBinaryGraph(Threshold) #0.15 the best so far
convertor.tonparyfloat(reader.X,reader.Y)
#convertor.Yratio()
convertor.storeNonZeros()
convertor.findTopRange()
convertor.emptyGraph()
#convertor.plotValueHistogram()
#convertor.originalMatrixplot(1000) #plot index 100 matrix
convertor.convert()
convertor.gatherEdgeInfoAbs()

#write each graph to json, create a dataset for graph2vec

os.system("cd /home/xiaomin/Desktop/node2vec/myGraph\nrm *")
os.system("cd /home/xiaomin/Desktop/node2vec/emb\nrm *")

for i in range(convertor.Xb.shape[0]):
    f= open(jsonfolder+str(i)+".json","w+")
    f1= open(embfolder+str(i)+".emb","w+")
    
    Gp = graph(convertor.Xb[i],convertor.X[i],convertor.edgeWeightSumPerNode[i])
    #Gp.plot()
    Gp.outemb(f1)
    f1.close()

    os.system("mv ../dataset/emb/"+str(i)+".emb /home/xiaomin/Desktop/node2vec/myGraph")
    os.system("cd /home/xiaomin/Desktop/node2vec\npython2 src/main.py --input myGraph/"+str(i)+".emb --output emb/"+str(i)+".emd --weighted")
    os.system("mv /home/xiaomin/Desktop/node2vec/emb/"+str(i)+".emd "+n2vfeaturefolder)

    f2 = open(n2vfeaturefolder+str(i)+".emd","r")
    Gp.getNodeFeatures(f2)
    f2.close()

    Gp.outjson(f)
    f.close()
    
    

print("done")
