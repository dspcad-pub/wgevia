-------------------------------------------------------------------------
@ddblock_begin copyright
----------------------------------------------------------------------------
Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
----------------------------------------------------------------------------
@ddblock_end copyright
-------------------------------------------------------------------------

WGEVIA: Weighted Graph Embedding with Vertex Identity Awareness.
This package is based on algorithms that are introduced in
[Wu 2021]. The package provides methods for graph-level embedding
of microcircuite data with potential applications to other 
types of data as well.

The wgevia package depends on the following DSPCAD Framework (DFW) package: DICE.

The wgevia package is a plug-in to the DICE package. Once DICE is started up in one's Bash environment, wgevia can be started up by running:

dxloadplugin dice wgevia

The command name prefix for this package is wgv.

For more information about installing and starting up DFW packages, see:

[1] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Base-Packages

[2] https://code.umd.edu/dspcad-pub/dspcadwiki/-/wikis/software/DSPCAD-Framework-Plug-In-Packages

The repository for downloading DICE is at:

[3] https://code.umd.edu/dspcad-pub/dice.git

Reference:

[Wu 2021] X. Wu, S. S. Bhattacharyya, and R. Chen. WGEVIA: A graph level
embedding method for microcircuit data. Frontiers in Computational
Neuroscience, 14:1-18, January 2021.

Release created on Fri Nov 19 22:43:28 EST 2021
