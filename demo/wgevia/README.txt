@ddblock_begin copyright
----------------------------------------------------------------------------
Copyright (c) 2017-2021
UMB-UMD Neuromodulation Research Group,
University of Maryland at Baltimore, and 
University of Maryland at College Park. 

All rights reserved.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.
----------------------------------------------------------------------------
@ddblock_end copyright


The demo can get you familiar with how to run WGEVIA and also can be used as a base if you plan to modify the codes. 
the data is provided for you in this demo, you can also follow the data format for your own input data.

How to run the demo:
step 1: create a directory with any name in any path, for example ~/wgevia_demo
step 2: $ wgvget wgevia
step 3: $ ./runme
step 4: find output embeddings in ./wgeviaout/embedding/
		find output classification info in ./wgeviaout/classification/
step 5: (if user want to test the inference function of wgevia) modify 'opcode: origin_10' in config.yaml to 'opcode: WGEVIAfastProject'
		and then
		$./runme
		inference results will be found in the same location.


For this demo, inputs are provided in correct format,
for preparing custom inputs, here are 
more info about input, output and file format:

1. Inputs:
Inputs required by WGEVIA software are a set of weighted graph data G = {g_1, g_2,...,g_m}. g_k is a weighted adjacency matrix.    
Totally m+1 files are needed. m is the number of graphs. All weighted graphs' adjacency matrix should be saved in m CSV files with naming data#.csv

A set of graphs' labels L = {l_1, l_2,...,l_m} is used in the classification stage of WGEVIA for supervised learning tasks. The label file is not needed 
if users utilize WGEVIA to generate embedding features for input graphs. All labels for weighted graphs should be put in file labelAll.csv.  
The indexing of labels in labelALl.csv file should be consistent with indexing of graph files data#.csv. For instance, the third label in 
labelAll.csv is the label for the graph saved in data3.csv.  
Without labelAll.csv, the downstream classifier will throw error, while the graph embedding results are already generated.
User can provide a dummy labelAll.csv with random labels for error free run. 

Before runing the software, copy all graph files and the label file to the following directory:   
wgevia_demo/MCSWE1.1origin/weightedGraphs

1.1. Input file format:
Example files of data#.csv and labelAll.csv are in the following directory:    
wgevia_demo/MCSWE1.1origin/weightedGraphs  

2. Outputs:
WGEVIA will output the generated graph embedding features as a CSV file. It will also report the classification results based on those generated graph 
features. 

Generated embedding features will be saved in the following file:  
wgevia_demo/wgeviaout/embedding/

Classification result based on generated graph features will be saved in the following file:  
wgevia_demo/wgeviaout/classification/ 

3. hyperparameter and run mode
User can find, study, and modify those configurations in wgevia_demo/config.yaml 
