#!/usr/bin/env bash

#############################################################################
# @ddblock_begin copyright

# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.

# @ddblock_end copyright 
#############################################################################

# Check that all relevant variables are defined

lxprog="wgevia-dev-wrapup-startup"

if [ -z "$UXNEURO" ]; then
    >&2 "$lxprog WARNING: UXNEURO varliable is undefined"
    >&2 "See the wgevia-dev/README.txt for more information"
fi

unset lxprog

# Terminate here if we are just starting up for administrative purposes
if [ -n "$UXWGEVIAADM" ]; then
    echo wrapup_startup: starting up for adminstrative purposes only
    echo ... not all features are enabled.
    return 0
fi

# install virtual environment for wgevia with conda.
# This command does nothing if the environment is already
# installed.

lxfile="$HOME/anaconda3/envs/wgeviaEnv"
if ! [ -a "$lxfile" ]; then
    conda env create --file $UXWGEVIA/config/env.yaml

    if [ $? -ne 0 ]; then
        >&2 echo wrapup_startup: problem configuring Python environment
        return 1
    fi

fi

# start virtual environment installed in anaconda
conda activate wgeviaEnv

if [ $? -ne 0 ]; then
    >&2 echo wrapup_startup: problem configuring Python environment
    return 1
fi
