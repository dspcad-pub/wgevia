#!/usr/bin/env python
# coding: utf-8

# In[1]:


################################################################################
# @ddblock_begin copyright
# -------------------------------------------------------------------------
# Copyright (c) 2017-2021
# UMB-UMD Neuromodulation Research Group,
# University of Maryland at Baltimore, and 
# University of Maryland at College Park. 
# 
# All rights reserved.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BALTIMORE
# OR UNIVERSITY OF MARYLAND COLLEGE PARK BE LIABLE TO ANY PARTY
# FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# 
# THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
# PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
# MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.DE MAINTENANCE, SUPPORT, UPDATES,
# ENHANCEMENTS, OR MODIFICATIONS.
# -------------------------------------------------------------------------

# @ddblock_end copyright
################################################################################

import numpy as np
import pandas as pd
import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--path', action="store", default='./',type=str)
parser.add_argument('--op', action="store", default='0',type=str)

args = parser.parse_args()

def outGraphCsv(graph,idx,path):
    df = pd.DataFrame(graph, columns = ['v1','v2','v3','v4','v5'])
    df.to_csv(path+'data'+str(idx)+'.csv')

# for simulate graph, test algorithm
def binaryAdjMatrixGen1(path):
    #path = ./dataset/
    #half labeled 0 half labeled 1

    numOfGraph = 1000
    numOfnode = 5
    
    
    
    file = open(path+"labelAll.csv","w")
    for i in range(numOfGraph):
        if i <numOfGraph/2:
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+1] = 1
            G[seed+1][seed] = 1
            

            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
        else:
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            G[seed][seed + 1] = 1
            G[seed+1][seed] = 1
            
            G[seed + 3][seed + 4] = 1
            G[seed + 4][seed +3] = 1
            

            outGraphCsv(G,i,path)

            file.write("1\n")

    file.close()

def binaryAdjMatrixGen2(path):
    #half labeled 0 half labeled 1

    numOfGraph = 1000
    numOfnode = 5


    file = open(path+"labelAll.csv","w")
    for i in range(numOfGraph):
        if i <numOfGraph/2:
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+3] = 1
            G[seed+3][seed] = 1
            

            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
        else:
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            
            G[seed + 1][seed + 4] = 1
            G[seed + 4][seed + 1] = 1

            outGraphCsv(G,i,path)
            file.write("1\n")

    file.close()

def binaryAdjMatrixGen3(path):
    #half labeled 0 half labeled 1

    numOfGraph = 1000
    numOfnode = 5


    file = open(path+"labelAll.csv","w")
    for i in range(numOfGraph):
        if i <numOfGraph/2:
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+1] = 1
            G[seed+1][seed] = 1
            
            G[seed][seed + 3] = 1
            G[seed+3][seed] = 1

            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
        else:
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            G[seed][seed + 1] = 1
            G[seed][seed + 2] = 1
            
            G[seed+1][seed] = 1
            G[seed+2][seed] = 1
            

            outGraphCsv(G,i,path)

            file.write("1\n")

    file.close()

def binaryAdjMatrixGen(path,op):
    #half labeled 0 half labeled 1

    numOfGraph = 500
    numOfnode = 5


    file = open(path+"labelAll.csv","w")
    for i in range(numOfGraph):
        if op=='s0':
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+1] = 1
            G[seed+1][seed] = 1
            
            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
            
        if op=='s1':
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            G[seed][seed + 1] = 1
            G[seed+1][seed] = 1
            
            G[seed + 3][seed + 4] = 1
            G[seed + 4][seed +3] = 1
            

            outGraphCsv(G,i,path)

            file.write("1\n")
        if op=='s2':
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+3] = 1
            G[seed+3][seed] = 1
            

            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
        if op=='s3':
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            
            G[seed + 1][seed + 4] = 1
            G[seed + 4][seed + 1] = 1

            outGraphCsv(G,i,path)
            file.write("1\n")
        if op=='s4':
            # generating 0-labeled graph
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 5,1)[0]
            seed = 0
            G[seed][seed+1] = 1
            G[seed+1][seed] = 1
            
            G[seed][seed + 3] = 1
            G[seed+3][seed] = 1

            outGraphCsv(G,i,path)
            # generating labels
            file.write("0\n")
        if op=='s5':
            G = np.zeros((numOfnode, numOfnode))
            #seed = np.random.choice(numOfnode - 1, 1)[0]
            seed = 0

            G[seed][seed + 1] = 1
            G[seed][seed + 2] = 1
            
            G[seed+1][seed] = 1
            G[seed+2][seed] = 1
            

            outGraphCsv(G,i,path)

            file.write("1\n")

    file.close()
    

path = args.path+'/'

if args.op == '1':
    binaryAdjMatrixGen1(path)
if args.op == '2':
    binaryAdjMatrixGen2(path)
if args.op == '3':
    binaryAdjMatrixGen3(path)
if args.op in ['s0','s1','s2','s3','s4','s5']:
    binaryAdjMatrixGen(path,args.op)
    


# In[ ]:




